import React,{Component} from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import * as action from "../../../store/actions/index"

class Logout extends Component {
    componentDidMount(){
        this.props.onAuthLogout()
    }

    render () {
        return <Redirect to='/' />;
    }
}

const mapDispatchToporps= dispatch=>{
    return {
        onAuthLogout: ()=>{dispatch(action.authertication_logout())}
    }
}

export default connect(null,mapDispatchToporps)(Logout);
