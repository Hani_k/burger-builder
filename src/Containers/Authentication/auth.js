import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

import Input from "../../Components/UI/Input/Input";
import Button from "../../Components/UI/Button/Button";
import Spinner from "../../Components/UI/Spinner/Spinner"
import classses from "./auth.module.css"
import Modal from "../../Components/UI/Modal/Modal";
import classes from "./auth.module.css";
import * as actions from "../../store/actions/index";


class Auth extends Component {

    state = {
        currentPass: '',
        singIn: true,
        signUpControls: {
            name: {
                inputType: 'input',
                label: 'name',
                inputConfig: {
                    type: 'text',
                    placeholder: 'Your Name'
                },
                validation: {
                    required: true,
                },
                value: '',
                valid: false,
                touched: false
            },
            email: {
                inputType: 'input',
                label: 'E-mail',
                inputConfig: {
                    type: 'text',
                    placeholder: ' email address'
                },
                validation: {
                    required: true,
                    isEmail: true
                },
                value: '',
                valid: false,
                touched: false
            },
            password: {
                inputType: 'input',
                label: 'password',
                inputConfig: {
                    type: 'password',
                    placeholder: 'password'
                },
                validation: {
                    required: true,
                    minLength: 7
                },
                value: '',
                valid: false,
                touched: false
            },
            password2: {
                inputType: 'input',
                label: 'Confirm password',
                inputConfig: {
                    type: 'password',
                    placeholder: 'Confirm password'
                },
                validation: {
                    required: true,
                    matchPass: true,
                },
                value: '',
                valid: false,
                touched: false
            }

        },
        controls: {
            email: {
                inputType: 'input',
                label: 'E-mail',
                inputConfig: {
                    type: 'text',
                    placeholder: ' email address'
                },
                validation: {
                    required: true,
                    isEmail: true
                },
                value: '',
                valid: false,
                touched: false
            },
            password: {
                inputType: 'input',
                label: 'password',
                inputConfig: {
                    type: 'password',
                    placeholder: 'password'
                },
                validation: {
                    required: true,
                    minLength: 7
                },
                value: '',
                valid: false,
                touched: false
            }
        }
    }

    componentDidMount(){
        if (!this.props.storeIsBuildingBurger && this.props.storeRedirectedPath ==='/checkout'){
            this.props.onSetAuthRedirectedPath('/')
        }
    }

    cheekFieldVaild = (field, rule) => {
        let isVaild = true;
        let errMes = '';
        if (rule.required) {
            isVaild = field.trim() !== '' && isVaild;
            errMes = isVaild ? '' : 'This field is rquired';
        }
        if (rule.minLength) {
            isVaild = field.length > rule.minLength && isVaild;
            errMes = isVaild ? '' : 'Too short';
        }
        if (rule.isEmail) {
            // const patrr = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            isVaild = pattern.test(field) && isVaild;
            errMes = isVaild ? '' : 'email : xx@yy.com'
        }
        if (rule.matchPass) {
            isVaild = field === this.state.currentPass && isVaild;
            errMes = isVaild ? '' : 'password dose not match';
        }
        return [isVaild, errMes]
    }

    inputChangeHandler = (event, elementID) => {
        let updatedControls = { ...this.state.controls };
        if (!this.state.singIn) {
            updatedControls = { ...this.state.signUpControls }
        }
        const changedElement = { ...updatedControls[elementID] };
        if (!this.state.singIn && changedElement.label === 'password') {
            this.setState({ currentPass: event.target.value })
        }
        const checkVaildity = this.cheekFieldVaild(event.target.value, changedElement.validation);
        changedElement.valid = checkVaildity[0];
        changedElement.errMess = checkVaildity[1];
        changedElement.value = event.target.value
        changedElement.touched = true;
        updatedControls[elementID] = changedElement;
        if (!this.state.singIn) {
            this.setState({ signUpControls: updatedControls })
        } else {
            this.setState({ controls: updatedControls })
        }
    }

    onSubmitHandler = (event) => {
        event.preventDefault()
        const authData = {}
        if (this.state.singIn) {
            for (let key in this.state.controls) {
                authData[key] = this.state.controls[key].value
            }
        } else {
            for (let key in this.state.signUpControls) {
                if (key === 'email' | key === 'password') {
                    authData[key] = this.state.signUpControls[key].value
                }
            }
        }
        const requestData = {
            ...authData,
            returnSecureToken: true
        }
        this.props.onAuthStarted(requestData, this.state.singIn)
    }

    ToggleSignInHandler = () => {
        this.setState((prevState) => {
            return {
                singIn: !prevState.singIn
            }
        })
    }

    render() {
        const authForm = [];
        if (!this.state.singIn) {
            for (let key in this.state.signUpControls) {
                authForm.push(
                    {
                        id: key,
                        config: this.state.signUpControls[key]
                    })
            }
        } else {
            for (let key in this.state.controls) {
                authForm.push(
                    {
                        id: key,
                        config: this.state.controls[key]
                    })
            }
        }
        const form = (
            <form onSubmit={this.onSubmitHandler}>
                {authForm.map(inputElement => {
                    return <Input
                        key={inputElement.id}
                        label={inputElement.config.label}
                        inputType={inputElement.config.inputType}
                        inputconfig={inputElement.config.inputConfig}
                        invalid={!inputElement.config.valid}
                        errMess={inputElement.config.errMess}
                        touched={inputElement.config.touched}
                        changed={(event) => { this.inputChangeHandler(event, inputElement.id) }}
                    />
                })}
                <Button className={'Success'}> {this.state.singIn ? 'SignIn' : 'Sign Up'}</Button>
                {this.state.singIn ?
                    <p>
                        <small>don't have account <button type="button" onClick={this.ToggleSignInHandler}>Signup</button></small>
                    </p>
                    : null}
            </form>
        );
        let error = null;
        if (this.props.storeError) {
            error = <small style={{ color: 'red', fontWeight: 'bolder' }}>{this.props.storeError}</small>
        }
        return ( 
            this.props.storeLoading ? < Spinner />
                :
                <Fragment>
                    {this.props.storeIsAuth ? <Redirect to={this.props.storeRedirectedPath}/> :null}
                    {this.state.singIn ?
                        <div className={classses.AuthForm}>
                            {error}
                            {form}
                        </div>
                        :
                        null}
                    <div className={classes.SingUpForm}>
                        <Modal hi showModal={!this.state.singIn} closeModal={this.ToggleSignInHandler}>
                            {error}
                            {form}
                        </Modal>
                    </div>
                </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        storeLoading: state.auth.loading,
        storeError: state.auth.error,
        storeIsAuth: state.auth.idToken !== null,
        storeRedirectedPath: state.auth.authRedirectedPath,
        storeIsBuildingBurger: state.burgerBuilder.building
    }
};

const mapDispathcToProps = dispatch => {
    return {
        onAuthStarted: (requestData, signInURL)=>{ dispatch(actions.authentication_started(requestData, signInURL)) },
        onSetAuthRedirectedPath: (path)=>{dispatch(actions.setAuthentucationRedirectPath(path))} 
    }
}

export default connect(mapStateToProps, mapDispathcToProps)(Auth);