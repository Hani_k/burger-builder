import React from "react";
import {configure, shallow} from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import {BurgerBuilder} from "./BurgerBuilder";
import BurgerControls from "../../Components/BuregerBuild/BurgerControls/BurgerControls"

configure({adapter:new Adapter()});

describe('< BurgerBuilder />', ()=>{
    let warpper;
    beforeEach(()=>{
        warpper = shallow(<BurgerBuilder onGetIngredients={()=>{}} />)
        //we have to pass onGetIngredients props here because it is expected to be there for componentDidMount
    });

    it('should have one control',()=>{
        warpper.setProps({storeIngs : {salad:0} })
        expect(warpper.find(BurgerControls)).toHaveLength(1)
    });
});