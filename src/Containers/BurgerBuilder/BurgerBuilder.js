import React, { Component, Fragment } from 'react';
import { connect } from "react-redux";

import Burger from "../../Components/BuregerBuild/BurgerBuild";
import BurgerControls from "../../Components/BuregerBuild/BurgerControls/BurgerControls";
import Modal from "../../Components/UI/Modal/Modal";
import OrderSummery from "../../Components/BuregerBuild/OrderSummery/OrderSummery";
// import axiosInstance from "../../axiosOrder";
import axios from "axios";
import Spinner from '../../Components/UI/Spinner/Spinner';
import withErrorHandler from "../../hoc/withErrorHandler/withErrorHandler";
import * as actionTypes from '../../store/actions/index';


export class BurgerBuilder extends Component {

  state = {
    startOrder: false,
    purshasability: false,
    loading: false,
  }

  componentDidMount() {
    this.props.onGetIngredients();
  }

  updateStateHandler = (ingredients) => {
    const ingredientsCount = Object.keys(ingredients).map(ing => {
      return (ingredients[ing])
    })
      .reduce((accumlative, current) => {
        return accumlative + current
      }, 0)
    const purshasability = ingredientsCount > 0
    return purshasability;
  }
  orderHandler = () => {
    if (this.props.storeIsAuth) {
      this.setState({ startOrder: true });
      this.props.onStartOrder()
    }
    else {
      this.props.onSetAuthRedirectedPath('/checkout')
      this.props.history.push({pathname:"/auth"})
    }
  }
  closeModalHandler = () => {
    this.setState({ startOrder: false })
  }
  continueOrederHandler = () => {
    this.props.history.push({
      pathname: "/checkout",
    });
  }

  render() {
    const disabeledInfo = { ...this.props.storeIngs }
    for (let key in disabeledInfo) {
      disabeledInfo[key] = disabeledInfo[key] > 0;
    }
    let burger = <Spinner />;
    let orderSummery = null;
    if (this.props.storeIngs) {
      burger = <Fragment>
        <Burger ingerdients={this.props.storeIngs} />
        <BurgerControls
          ingerdients={this.props.storeIngs}
          price={this.props.storePrice}
          purshasability={this.updateStateHandler(this.props.storeIngs)}
          showModal={this.orderHandler}
          disabeled={disabeledInfo}
          add={this.props.onIcnreaseIng}
          remove={this.props.onDecreaseIng} />
      </Fragment>
      orderSummery = <OrderSummery
        ingerdients={this.props.storeIngs}
        closeModal={this.closeModalHandler}
        continueOrder={this.continueOrederHandler} />
        ;
    }
    if (this.props.storeError) {
      const errorMessage = <p style={{
        color: "red",
        textAlign: "center",
        textTransform: "uppercase",
        fontWeight: "bolder"
      }}>Somthing went wrong !!{/*<br/>{this.state.errors}*/} </p>
      orderSummery = errorMessage;
      burger = errorMessage;
    } if (this.state.loading) {
      orderSummery = <Spinner />
    }
    return (
      <Fragment>
        <Modal showModal={this.state.startOrder} closeModal={this.closeModalHandler}>
          {orderSummery}
        </Modal>
        {burger}
      </Fragment>
    );
  }

}
const mapStateToProps = state => {
  return {
    storeIngs: state.burgerBuilder.ingredients,
    storePrice: state.burgerBuilder.price,
    storeError: state.burgerBuilder.error,
    storeIsAuth: state.auth.idToken !== null
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onGetIngredients: () => dispatch(actionTypes.getIngredients()),
    onIcnreaseIng: (Ing) => dispatch(actionTypes.increaseIng(Ing)),
    onDecreaseIng: (Ing) => dispatch(actionTypes.decreaseIng(Ing)),
    onStartOrder: () => dispatch(actionTypes.orderStartInit()),
    onSetAuthRedirectedPath: (path)=>{dispatch(actionTypes.setAuthentucationRedirectPath(path))}
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(BurgerBuilder, axios));