import React, { Component } from "react";
import {connect} from "react-redux";

import classes from "./ContactFrom.module.css";
import Button from '../../../Components/UI/Button/Button';
import axiosInstance from "../../../axiosOrder";
import Spinner from "../../../Components/UI/Spinner/Spinner";
import Input from "../../../Components/UI/Input/Input";
import withErrorHandler from "../../../hoc/withErrorHandler/withErrorHandler";
import * as actions from "../../../store/actions/index";
import { Redirect } from "react-router-dom";


class ContactFrom extends Component {

  state = {
    loading: false,
    wholeFormValid: false,
    orderForm: {
      name: {
        inputType: 'input',
        label: 'Name',
        inputConfig: {
          type: 'text',
          placeholder: 'Your Name'
        },
        validation:{
          required:true,
        },
        value:'',
        valid:false,
        touched:false
      },
      email: {
        inputType: 'input',
        label: 'Email',
        inputConfig: {
          type: 'text',
          placeholder: 'E-mail'
        },
        validation:{
          required:true,
        },
        value:'',
        valid:false,
        touched:false
      },
      phone: {
        inputType: 'input',
        label: 'Phone',
        inputConfig: {
          type: 'number',
          placeholder: 'Your Phone Number'
        },
        validation:{
          required:true,
          minLength: 11,
          maxLength: 13,
        },
        errMess:'',
        value:'',
        valid:false,
        touched:false
      },
      country: {
        inputType: 'input',
        label: 'Country',
        inputConfig: {
          type: 'text',
          placeholder: 'Your Country'
        },
        validation:{
          required:true,
        },
        value:'',
        valid:false,
        touched:false
      },
      TransferMeethod: {
        inputType: 'select',
        label: 'Transfer method',
        inputConfig: {
          option: [
            {value: '', displayValue: '.... '},
            {value: 'fastest', displayValue: 'Fastest' },
            {value: 'cheapest', displayValue: 'Cheapest'}
            
          ]
        },
        validation:{
          required:true,
        },
        value:'fastest',
        valid:false,
        touched:false
      }
    }
  }

  cheekFieldVaild = (field, rule)=>{
    let isVaild = true;
    let errMess = '';
    if (rule.required){
      isVaild = field.trim() !== '' && isVaild;
      errMess = isVaild? '': 'this field is requird.'
    }
    if (rule.minLength){
      let len = field.length
      isVaild = len > 10 && isVaild;
      errMess = isVaild? '': 'Not vaild Number';
    }
    if (rule.maxLength){
      let len = field.length
      isVaild = len < 14 && isVaild;
      errMess = isVaild? '': 'Not vaild Number'
    }
    return [isVaild, errMess];
  }

  inputChangeHandler = (e, fieldId)=>{
    // console.log( e.currentTarget,fieldId)
    const updatedOrderForm = {...this.state.orderForm}
    let currentElement = {...updatedOrderForm[fieldId]}
    currentElement.value = e.target.value
    const currentElementValidationRules = currentElement.validation
    const checkFuc = this.cheekFieldVaild(currentElement.value, currentElementValidationRules)
    currentElement.valid = checkFuc[0]
    currentElement.errMess = checkFuc[1]
    currentElement.touched = true;
    updatedOrderForm[fieldId] = currentElement;

    let wholeForm = true;
    for (let field in updatedOrderForm){
      wholeForm = updatedOrderForm[field].valid && wholeForm;
    }
    this.setState({orderForm:updatedOrderForm, wholeFormValid:wholeForm})
  }

  orderHandler = (event) => {
    event.preventDefault()
    // this.setState({ loading: true });
    const contact = {};
    for (let key in this.state.orderForm){
      contact[key] = this.state.orderForm[key].value
    }
    const order = {
      userID: this.props.storeUserId,
      ingredients: this.props.ingredients,
      price: this.props.price,
      contact:contact
    }
    this.props.onSendOrd(order, this.props.storeIdToken)
    // axiosInstance.post('https://react-burger-10811.firebaseio.com/Orders.json', order)
    //   .then(requse => {
    //     this.setState({ loading: false });
    //     this.props.history.replace("/")
    //   })
    //   .catch(err => {
    //     console.log(err)
    //     //this.setState({ errors: err.message , startOrder:false})
    //   })
  }
  render() {
    const orderForm = [];
    for (let key in this.state.orderForm) {
      orderForm.push({
        id: key,
        config: this.state.orderForm[key]
      });
    }
    let form = (
      <form onSubmit={this.orderHandler}>
        {orderForm.map(inputElement => {
          return <Input
            key={inputElement.id}
            label={inputElement.config.label}
            inputType={inputElement.config.inputType}
            inputconfig={inputElement.config.inputConfig}
            invalid={!inputElement.config.valid}
            errMess={inputElement.config.errMess}
            touched={inputElement.config.touched}
            changed={(event)=>{this.inputChangeHandler(event, inputElement.id)}}
            // placeholder={inputElement.config.inputConfig.placeholder}
            // type={inputElement.config.type}
          />
        })}
        <Button className={'Success'} wholeFormValid={! this.state.wholeFormValid}> Order</Button>
      </form>
    )
    if (this.props.storeLoading) {
      form = (<div className={classes.Spinner}>
        <Spinner noMarginLeft />
      </div>)
    }
    if (this.props.storePurshased){
      form = <Redirect to="/" />
    }

    return (
      <div className={classes.ContactFrom}>
        {form}
      </div>
    );
  }
}

const mapStateToProps = state=>{
  return {
    storeLoading: state.order.loading,
    storePurshased:state.order.purshased,
    storeIdToken:state.auth.idToken,
    storeUserId:state.auth.localID
  }
};

const mapDispatchToProps = dispatch=>{
  return{
    onSendOrd:  (ordData,token)=>dispatch(actions.orderStarted(ordData, token))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(ContactFrom, axiosInstance));