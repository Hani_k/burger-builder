import React, { Component } from "react";
import { connect } from "react-redux";

import { Route, Redirect } from "react-router-dom"
import Burger from "../../Components/BuregerBuild/BurgerBuild";
import classes from "./CheckOut.module.css";
import ContactForm from "./ContactForm/ContactFrom";
import Button from "../../Components/UI/Button/Button";
// import Spinner from "../../Components/UI/Spinner/Spinner";


class CheckOut extends Component {


  // constructor(props) {
  //   super(props)

  //   // const ingerdients = {};
  //   // let price = 0;
  //   // const query = new URLSearchParams(this.props.location.search);
  //   // for (let param of query.entries()) {
  //   //   if (param[0] === 'price') {
  //   //     price = +param[1]
  //   //   } else {
  //   //     ingerdients[param[0]] = +param[1]
  //   //   }
  //   // }
  //   this.state = {
  //     ingerdients: ingerdients,
  //     price: price,
  //     loading: false
  //   };
  // }

  canelOreder = () => {
    this.props.history.goBack()
  }

  continueOrder = () => {
    this.props.history.replace('/checkout/orderForm')
  }
  render() {
    let checkout = null;
    if (!this.props.storeIngs) {
      checkout = <Redirect to="/" />;
    }
    else {
      checkout = (<div className={classes.CheckOut}>
        <h2> We hope it teast well</h2>
        <Burger ingerdients={this.props.storeIngs} />
        <p>Total price : {this.props.storePr.toFixed(2)}</p>
        <div className={classes.Center}>
          <Button
            className="Danger"
            clicked={this.canelOreder}>CANCEL</Button>
          <Button
            className="Success"
            clicked={this.continueOrder}>CONTINUE</Button>
        </div>
        <Route
          path={this.props.match.path + "/orderForm"} exact
          render={(props) => (<ContactForm ingredients={this.props.storeIngs}
            price={this.props.storePr} {...props} />)} />
      </div>);
    }
    return (
      checkout
    );
  }
}

const mapStateToProps = state => {
  return {
    storeIngs: state.burgerBuilder.ingredients,
    storePr: state.burgerBuilder.price
  }
};


export default connect(mapStateToProps)(CheckOut);