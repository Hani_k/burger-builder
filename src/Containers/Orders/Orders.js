import React, { Component } from "react";
import { connect } from "react-redux";

import axiosInstance from "../../axiosOrder";
import Order from './Order/Order';
import Spinner from "../../Components/UI/Spinner/Spinner";
import withErrorHandler from "../../hoc/withErrorHandler/withErrorHandler";
import * as action from '../../store/actions/index';

class Orders extends Component {
  state = {
    loading: false,
    orders: []
  }

  componentDidMount() {
    // this.setState({ loading: true })
    // const orders = [];
    // axiosInstance.get("https://react-burger-10811.firebaseio.com/Orders.json")
    //   .then(res => {
    //     this.setState({ loading: false })
    //     for (let keyOrd in res.data) {
    //       res.data[keyOrd].key = keyOrd;
    //       orders.push(res.data[keyOrd])
    //     }
    //     this.setState({ orders: orders })
    //   })
    //   .catch (err => {
    //     console.log(err)
    //   })
    this.props.onFetchOrd(this.props.storeIdToken, this.props.storeUserID)
  }

  render() {
    return (
      this.props.storeLoading ? <Spinner />
        : this.props.soterOrd.map((ord,i) => {
          return (
            <Order
              key={ord.key}
              number={i}
              cheese={ord.ingredients.cheese}
              chiken={ord.ingredients.chiken}
              meat={ord.ingredients.meat}
              salad={ord.ingredients.salad}
              price = {ord.price} />
          );
        })
    );
  }
};

const mapStateToProps = state=>{
  return {
    soterOrd:state.order.orders,
    storeLoading:state.order.loading,
    storeIdToken:state.auth.idToken,
    storeUserID:state.auth.localID
  }
};

const mapDispatchToProps = dispatch=>{
  return {
    onFetchOrd: (token, userID)=>{dispatch(action.fetchOrderStarted(token, userID))}
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(Orders, axiosInstance));
