import React from "react";

import classes from "./Order.module.css";

const Order = (props)=>{
  return(
    <div className={classes.Order}>
      <ul className={classes.Card}>
      <h2>order : {props.number}</h2>
        <li>cheese : {props.cheese}</li>
        <li>chiken : {props.chiken}</li>
        <li>meat : {props.meat}</li>
        <li>salad : {props.salad}</li>
        <li>Price: {props.price.toFixed(2)}</li>
      </ul>
    </div>
  );
};
export default Order;