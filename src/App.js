import React, { Component, Suspense } from 'react';
import { Route, Switch, Redirect } from "react-router-dom";
import {connect} from "react-redux";

import asynComponent from "./hoc/asyncComponent/asyncComponent";
import Layout from './hoc/Layout/Layout';
import BurgerBuilder from './Containers/BurgerBuilder/BurgerBuilder';
import CheckOut from "./Containers/CheckOut/CheckOut";
//import Orders from "./Containers/Orders/Orders";
//import Auth from "./Containers/Authentication/auth";
import Logout from "./Containers/Authentication/logout/logout";
import Spinner from './Components/UI/Spinner/Spinner';
import * as action from "./store/actions/index";
import './App.css';


// USEING OUR OWNE HOC Hiegher Oreder Compnoent//
const ayscOreders = asynComponent(()=>{
  return import ("./Containers/Orders/Orders")  
});

// USEING React ready to use Hiegher Oreder Compnoent//
const AysncAuth = React.lazy(()=>{
  return import ("./Containers/Authentication/auth")
})

class App extends Component {

  componentDidMount(){
    this.props.onCheckLocalStorgeAuthState()
  }

  render() {
    let route= null;
    if (this.props.storeIsAuth){
      route = (<Switch>
          <Route path="/" exact component={BurgerBuilder} />
          <Route path="/orders"  component={ayscOreders} />
          <Route path="/logout"  component={Logout} />
          <Route path="/checkout"  component={CheckOut} />
          <Route path="/auth"  render={()=>{return <Suspense fallback={<Spinner/>}><AysncAuth /></Suspense>}} />
          <Redirect to="/"/>
        </Switch>);
    }else {
      route = (
        <Switch>
          <Route path="/" exact component={BurgerBuilder} />
          <Route path="/auth"  render={()=>{return <Suspense fallback={<Spinner/>}><AysncAuth /></Suspense>}} />
          <Redirect to="/"/>
        </Switch>
      );
    }
    return (
      <Layout >
       {route}
      </Layout>
    );
  }
}

const mapStateToprops = state =>{
  return{
    storeIsAuth: state.auth.idToken !== null
  }
};

const mapDispatchToProps = dispatch=>{
  return{
    onCheckLocalStorgeAuthState: ()=>{dispatch(action.checkLocalStorgeAuthState())}
  }
}
export default connect(mapStateToprops, mapDispatchToProps)(App);
