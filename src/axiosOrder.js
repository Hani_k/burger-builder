// import React from "react";
import axios from "axios";

const axiosInstance = axios.create({
  baseURL:'https://react-burger-10811.firebaseio.com'
}
)

export default axiosInstance;