import React from "react";

import classes from "./Input.module.css";

const Input = (props) => {
    let inputClass = [classes.InputEle]
    let errMess = '';
    if (props.invalid & props.touched) {
        inputClass.push(classes.Invalid)
        errMess = <small style={{color:'red'}}>{props.errMess}</small>;
    }
    inputClass = inputClass.join(' ');
    let inputElement = null;
    switch (props.inputType) {
        case 'input':
            inputElement = <input
                className={inputClass}
                {...props.inputconfig}
                onChange={props.changed} />
            break;
        case 'select':
            inputElement = (
                <select
                    className={inputClass}
                    onChange={props.changed}>
                    {props.inputconfig.option.map(option => {
                        return (
                            <option
                                key={option.value}
                                value={option.value}>
                                {option.displayValue}
                            </option>);
                    })}
                </select>)
            break;
        default:
            inputElement = <input className={inputClass} {...props.inputconfig} />
    }
    return (
        <div className={classes.FormGroub}>
            <label>{props.label}</label>
            {inputElement}
            <div className={classes.ErrorMess}>{errMess}</div>
        </div>
    );
}

export default Input;
