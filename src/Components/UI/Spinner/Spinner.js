import React from "react";
import classes from "./Spinner.module.css";

const Spinner = (props) => (
  <div className={classes.ldsRoller}
  style={props.noMarginLeft?{marginLeft:"0"}:{marginLeft:"40%"}}><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
);

export default Spinner;