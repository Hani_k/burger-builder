import React from "react";

import classes from "./Button.module.css"

const Button = (props) => (
  <button
    className={[classes.Button, classes[props.className]].join(' ')}
    onClick={props.clicked}
    disabled={props.wholeFormValid}>
    {props.children}
  </button>
);

export default Button;