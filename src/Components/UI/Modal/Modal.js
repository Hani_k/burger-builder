import React, { Fragment, useEffect } from "react";

import classes from "./Modal.module.css";
import Backdrop from "../Backdrop/Backdrop";

const Modal = (props) => {
  useEffect((s)=>{
  }, [props.showModal])
  /* We alsoe could used class based component wiht component
   should update method and check if the showModal props has changed*/
  if (props.showModal) {
    return (
      <Fragment>
        <Backdrop show={props.showModal} hideModal={props.closeModal} />
        <div
          className={classes.Modal}
          style={{
            transform: props.showModal? 'translateY(0vh)' : 'translateY(-100vh)',
            opacity: props.showModal ? '0.9' : '0',
            top: props.hi ? '10%' : '30%',
          }}
        >
          {props.children}
        </div>
      </Fragment>
    )
  } else
    return null;
}
export default React.memo(Modal);