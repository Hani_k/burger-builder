import React   from "react";

import calsses from "./ToolBar.module.css";
import NavigationItems from "../NavigationItems/NavigationItems";
import Logo from "../../Logo/Logo";
import SideDrawToggler from "../SideDrawer/SideDrawToggler/SideDrawToggler"

const ToolBar = (props) => ( 
  <header className={calsses.ToolBar}>
    {props.showMenuBtn? <SideDrawToggler cilcked={props.ToggelSideDrawer}/>:null}
    <Logo />
    <nav className={calsses.DesktopOnly}>
    <NavigationItems isAuthenticated={props.isAuth}/>
    </nav>
  </header>
);

export default ToolBar;