import React from "react";

import classes from './NavigationItems.module.css';
import NavigationItem from "./NavigationItem/NavigationItem";

const NavigationItems = (props) => {
  return (
    <div className={classes.NavigationItems}>
      <NavigationItem link='/' >Home</NavigationItem>
      {props.isAuthenticated? <NavigationItem link='/orders' >Orders</NavigationItem> : null}
      {props.isAuthenticated ? 
      <NavigationItem link='/logout'>logout</NavigationItem>
      : 
      <NavigationItem link='/auth' >Login</NavigationItem>}
    </div>
  );
};

export default NavigationItems;