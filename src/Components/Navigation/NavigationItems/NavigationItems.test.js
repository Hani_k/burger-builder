import React from "react";
import {configure, shallow} from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import NavigationItems from "./NavigationItems";
import NavigationItem from "./NavigationItem/NavigationItem";

configure ({adapter: new Adapter()});

describe('< NavigationItems />', ()=>{
    let wraper
    beforeEach(()=>{
         wraper = shallow(< NavigationItems />);
    });
    
    it('should render 2 navigation item', ()=>{
        expect(wraper.find(NavigationItem)).toHaveLength(2);
    });
    
    it('should render 3 navigation item', ()=>{
        //wraper = shallow(< NavigationItems isAuthenticated/>);
        wraper.setProps({isAuthenticated:true})
        expect(wraper.find(NavigationItem)).toHaveLength(3);
    });
    
    it('should render LOGOUT link', ()=>{
        //wraper = shallow(< NavigationItems isAuthenticated/>);
        wraper.setProps({isAuthenticated:true})
        expect(wraper.contains(<NavigationItem link='/logout'>logout</NavigationItem>)).toEqual(true);
    });

});