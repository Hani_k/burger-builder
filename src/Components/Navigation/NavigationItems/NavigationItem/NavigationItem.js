import React from "react";

import { NavLink } from "react-router-dom";
import classes from "./NavigationItem.module.css"
const NavigationItem = (props)=>(
  <div className={classes.NavigationItem}>
    <NavLink 
    exact
    activeClassName={classes.active}
    //className={props.active? classes.active:''}
    to={props.link}>{props.children}</NavLink>
  </div>
);

export default NavigationItem;