import React, { Fragment } from "react";

import classes from "./SideDrawer.module.css";
import NavigationItems from "../NavigationItems/NavigationItems";
import Logo from "../../Logo/Logo";
import Backdrop from "../../UI/Backdrop/Backdrop";

const SideDrawer = (props) => {
  let classChain = [classes.SideDrawer, classes.Close]
  if (props.showSideDrawer){
    classChain = [classes.SideDrawer, classes.Open]
  }
  return (
    <Fragment>
      {props.showSideDrawer?<Backdrop show hideModal={props.closeSideDrawr}/>:null}
      <div className={classChain.join(' ')}>
        <div className={classes.Logo}>
          <Logo />
        </div>
        <NavigationItems  isAuthenticated={props.isAuth}/>/>
      </div>
    </Fragment>
  );
};

export default SideDrawer;