import React from "react";

import Button from "../../UI/Button/Button";

const OrderSummery = (props) => {
  const ingredients = Object.keys(props.ingerdients).map(ing => {
    return <li key={ing}>
      <p><strong style={{ textTransform: 'capitalize' }}>{ing} : </strong> {props.ingerdients[ing]}</p>
    </li>
  })
  return (
    <div>
      <h2>Your meal contains :</h2>
      <ul>
        {ingredients}
      </ul>
      <p>Continue to checkout ?</p>
      <Button className='Danger' clicked={props.closeModal}>CANCEL</Button>
      <Button className='Success' clicked={props.continueOrder}>COUNTINUE</Button>
    </div>
  );
};

export default OrderSummery