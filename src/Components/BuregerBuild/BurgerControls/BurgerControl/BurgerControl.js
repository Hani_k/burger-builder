import React from "react";

import classes from "./BurgerControl.module.css"

const BurgerControl = (props)=>{
  return (
    <div className={classes.BurgerControl}>
      <div className={classes.Label}>{`${props.ingredient}`.toLocaleUpperCase()}</div>
      <button className={classes.Less} onClick={props.remove.bind(this, props.ingredient)} disabled={ !props.disabel}>LESS</button>
      <button className={classes.More} onClick={props.add.bind(this, props.ingredient)}>MORE</button>
    </div>
  );

};
export default BurgerControl;