import React from "react";

import classes from "./BurgerControls.module.css";
import BurgerControl from "./BurgerControl/BurgerControl"


const BurgerControls = (props) => {
  const ingredient = Object.keys(props.ingerdients)
    .map((ing, index) => {
      return <BurgerControl
        key={ing + index}
        ingredient={ing}
        add={props.add}
        remove={props.remove}
        disabel={props.disabeled[ing]}
      />
    })
  return (
    <div className={classes.BurgerControls}>
      <h3>Total price is : {props.price.toFixed(2)}$</h3>
      {ingredient}
      <button 
      className={classes.OrderButton} 
      disabled={! props.purshasability}
      onClick={props.showModal}
      >OREDER</button>
    </div>
  );

};

export default BurgerControls