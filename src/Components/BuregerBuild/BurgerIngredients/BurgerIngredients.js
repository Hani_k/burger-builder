import React, { Component } from "react";
import clases from "./BurgerIngredients.module.css"

class BurgerIngredients extends Component {

  render() {
    let ingerdients = null;
    switch (this.props.type) {
      case ('bread-bottom'):
        ingerdients = <div className={clases.BreadBottom}></div>;
        break;
      case ('bread-top'):
        ingerdients = <div className={clases.BreadTop}>
          <div className={clases.Seeds1}></div>
          <div className={clases.Seeds2}></div>
        </div>
        break;
      case ('salad'):
        ingerdients = <div className={clases.Salad}></div>
        break;
      case ('meat'):
        ingerdients = <div className={clases.Meat}></div>
        break;
      case ('cheese'):
        ingerdients = <div className={clases.Cheese}></div>
        break;
      case ('chiken'):
        ingerdients = <div className={clases.Chiken}></div>
        break;
      default:
        return null;
    }
    return ingerdients;

  }
}

export default BurgerIngredients;