import React from "react";

import clases from "./BurgerBuild.module.css";
import BurgerIngerdients from "./BurgerIngredients/BurgerIngredients"

const BurgerBuild = (props) => {
  let ingredients = Object.keys(props.ingerdients)
    .map((ingredient) => {
      return [...Array(props.ingerdients[ingredient])].map((_, index)=>{
        return <BurgerIngerdients type={ingredient} key={ingredient+index}/>
      })
    })
    .reduce((prev, current)=>{
      return prev.concat(current)
    },[])
    ;
    if ( ingredients.length === 0){
      ingredients = <p>Add your faveurate ingredients</p>
    }
  return (
    <div className={clases.Burger}>
      <BurgerIngerdients type='bread-top' />
      {ingredients}
      <BurgerIngerdients type='bread-bottom' />
    </div>
  )
};

export default BurgerBuild