import React, { Fragment, Component } from 'react';
import { connect } from "react-redux";

import classes from './Layout.module.css';
import ToolBar from "../../Components/Navigation/ToolBar/ToolBar";
import SideDrawer from "../../Components/Navigation/SideDrawer/SideDrawer"

class Layout extends Component {
  state = {
    showSideDrawer: false
  }

  ToggelSideDrawer = () => {
    this.setState((prevState, prevProps) => {
      return { showSideDrawer: !prevState.showSideDrawer }
    })
  }

  closeSideDrawe = () => {
    this.setState({ showSideDrawer: false })
  }

  render() {
    return (
      <Fragment>
        <ToolBar
          isAuth={this.props.storeIsAuth}
          ToggelSideDrawer={this.ToggelSideDrawer}
          showMenuBtn={!this.state.showSideDrawer} />
        <SideDrawer
          isAuth={this.props.storeIsAuth}
          showSideDrawer={this.state.showSideDrawer}
          closeSideDrawr={this.closeSideDrawe} />
        <main className={classes.Content}>
          {this.props.children}
        </main>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    storeIsAuth: state.auth.idToken !== null
  }
};

export default connect(mapStateToProps)(Layout);
