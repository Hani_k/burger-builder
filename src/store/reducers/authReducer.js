import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "./utility";

const initialState = {
    idToken: null,
    localID: null,
    loading: false,
    error: null,
    authRedirectedPath: '/'
};

const authenticationStart = (state, action) => {
    return updateObject(state, {
        loading: true,
        error: null
    })
};

const authenticationSuccessful = (state, action) => {
    return updateObject(state, {
        loading: false,
        idToken: action.idToken,
        localID: action.localId
    })
};
const authenticationFaild = (state, action) => {
    return updateObject(state, {
        loading: false,
        error: action.errMess
    })
};

const autherticationLogout = (state, action) => {
    return updateObject(state, {
        idToken: null,
        localID: null,
    })
};

const setAuthRedirectPath = (state, action) => {
    return updateObject(state, {
        authRedirectedPath:action.path
    })
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.AUTHENTICATION_START: return (authenticationStart(state, action))
        case actionTypes.AUTHENTICATION_SUCCESSFUL: return (authenticationSuccessful(state, action))
        case actionTypes.AUTHENTICATION_FAILD: return (authenticationFaild(state, action))
        case actionTypes.AUTHENTICATION_LOGOUT: return (autherticationLogout(state, action))
        case actionTypes.SET_AUTHENTICATION_REDIRECT_PATH: return (setAuthRedirectPath(state, action))
        default: return state
    }
};

export default reducer;