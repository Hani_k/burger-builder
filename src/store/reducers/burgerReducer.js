import * as  actionTypes from "../actions/actionTypes";
import { updateObject } from "./utility";

const initState = {
    ingredients: null,
    price: 4.00,
    error: false,
    building: false
};

const ingredientPrices = {
    salad: 0.5,
    cheese: 0.6,
    meat: 1.3,
    chiken: 1.0
}

const getIngredients = (state, action) => {
    return updateObject(state, {
        ingredients: {
            salad: action.ingredients.salad,
            cheese: action.ingredients.cheese,
            meat: action.ingredients.meat,
            chiken: action.ingredients.chiken,
        },
        price: 4.00,
        error: false,
        building: false
    })
};

const getIngredientsFaild = (state, action) => {
    return updateObject(state, { error: true })
};

const increaseIngerdient = (state, action) => {
    return updateObject(state, {
        ingredients: {
            ...state.ingredients,
            [action.payload.ingredient]: state.ingredients[action.payload.ingredient] + 1
        },
        price: state.price + ingredientPrices[action.payload.ingredient],
        building: true
    })
};

const decreaseIngredient = (state, action) => {
    return updateObject(state, {
        ingredients: {
            ...state.ingredients,
            [action.payload.ingredient]: state.ingredients[action.payload.ingredient] - 1
        },
        price: state.price - ingredientPrices[action.payload.ingredient],
        building: true
    })
};

const reducer = (state = initState, action) => {
    switch (action.type) {
        case actionTypes.GET_INGREDIENTS: return getIngredients(state, action)
        case actionTypes.GET_INGREDIENTS_FAILD: return getIngredientsFaild(state, action)
        case actionTypes.INCREASE_ING: return increaseIngerdient(state, action)
        case actionTypes.DECREASE_ING: return decreaseIngredient(state, action)
        default: return state
    }
};

export default reducer;