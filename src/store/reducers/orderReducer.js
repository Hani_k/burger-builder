import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "./utility";

const initState = {
    orders: [],
    error: false,
    loading: false,
    purshased: false
};

const orderStartInit = (state, action) => {
    return updateObject(state, { purshased: false })
};

const orderStart = (state, action) => {
    return updateObject(state, { loading: true })
};

const orderSuccessful = (state, action) => {
    return updateObject(state, {
        // orders: state.orders.concat({
        //     ...action.order,
        // }),
        loading: false,
        purshased: true
    })
};

const orderFaild = (state, action) => {
    return updateObject(state, {
        loading: false,
        error: true
    })
};

const fetchOrderStart = (state, action) => {
    return updateObject(state, { loading: true })
};

const fetchOrderSuccessfull = (state, action) => {
    return updateObject(state, {
        orders: action.orders,
        loading: false
    })
};

const fetchOrderFaild = (state, action) => {
    return updateObject(state, {
        error: action.error,
        loading: false
    })
};
const reducer = (state = initState, action) => {
    switch (action.type) {
        case actionTypes.OREDER_START_INIT: return orderStartInit(state, action)
        case actionTypes.OREDER_START: return orderStart(state, action)
        case actionTypes.ORDER_SUCCESSFUL: return orderSuccessful(state, action)
        case actionTypes.ORDER_FAILD: return orderFaild(state, action)
        case actionTypes.FETCH_ORDER_START: return fetchOrderStart(state, action)
        case actionTypes.FETCH_ORDER_SUCCESSFULL: return fetchOrderSuccessfull(state, action)
        case actionTypes.FETCH_ORDER_FAILD: return fetchOrderFaild(state, action)
        default: return state;
    }
};

export default reducer;