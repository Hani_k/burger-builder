import {configure} from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import reducer from "./authReducer";
import * as  actionTypes from "../actions/actionTypes"

configure({adapter:new Adapter()});

describe('test Authentication Reducer', ()=>{
    it('should return the initState', ()=>{
        expect(reducer(undefined,{})).toEqual({
            idToken: null,
            localID: null,
            loading: false,
            error: null,
            authRedirectedPath: '/'
        })
    });

    it('should set loading to true',()=>{
        expect(reducer({
            idToken: null,
            localID: null,
            loading: false,
            error: null,
            authRedirectedPath: '/'
        },{type: actionTypes.AUTHENTICATION_START})).toEqual({
            idToken: null,
            localID: null,
            loading: true,
            error: null,
            authRedirectedPath: '/'
        })
    })
});