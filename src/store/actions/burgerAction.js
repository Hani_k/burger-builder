import axios from "axios";

import * as actionTypes from './actionTypes'

export const fectchIngredients = (fetchedIngredints) => {
    return {
        type: actionTypes.GET_INGREDIENTS,
        ingredients: { ...fetchedIngredints }
    }
};

export const fetchIngsFaild = ()=>{
    return {
        type:actionTypes.GET_INGREDIENTS_FAILD
    }
};

export const getIngredients = () => {
    return dispatch => {
        axios.get('https://react-burger-10811.firebaseio.com/Ingredients.json')
            .then(response => {
                dispatch(fectchIngredients(response.data))
            })
            .catch(error => {
                dispatch(fetchIngsFaild())
            })
    }
};

export const increaseIng= (Ing)=>{
    return{
        type:actionTypes.INCREASE_ING,
        payload:{
            ingredient:Ing,
        }

    }
};

export const decreaseIng= (Ing)=>{
    return{
        type:actionTypes.DECREASE_ING,
        payload:{
            ingredient:Ing
        }

    }
};