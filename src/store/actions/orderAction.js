import * as actionTypes from "./actionTypes";
import axiosInstance from "../../axiosOrder";

export const orderStartInit = ()=>{
    return {
        type: actionTypes.OREDER_START_INIT
    }
};

export const orderStart = ()=>{
    return {
        type:actionTypes.OREDER_START
    }
};

export const orderSuccessful = (ordID, ordData)=>{
    return {
        type:actionTypes.ORDER_SUCCESSFUL,
        order:{
            id:ordID,
            ordData:ordData
        }
    }
};

export const orderFaild = (error)=>{
    return {
        type:actionTypes.ORDER_FAILD,
        error: error
    }
};

export const orderStarted = (orderData, token) => {
    return dispatch => {
        dispatch(orderStart())
        axiosInstance.post('https://react-burger-10811.firebaseio.com/Orders.json?auth='+token, orderData)
        .then(requse => {
            dispatch(orderSuccessful(requse.data.name, orderData))
        })
        .catch(err => {
            dispatch(orderFaild(err))
        })
    }
};

export const fetchOrederStart = ()=>{
    return {
        type:actionTypes.FETCH_ORDER_START
    }
};

export const fetchOrederSuccessfull = (orders)=>{
    return {
        type:actionTypes.FETCH_ORDER_SUCCESSFULL,
        orders: orders
    }
};

export const fetchOrderFaild = (err)=>{
    return {
        type:actionTypes.FETCH_ORDER_FAILD,
        error :err
    }
};

export const fetchOrderStarted = (token, userID)=>{
    return dispatch=>{
        dispatch(fetchOrederStart())
        const orders = [];
        const queryParams = "?auth="+token + '&orderBy="userID"&equalTo="'+userID+'"';
        axiosInstance.get("https://react-burger-10811.firebaseio.com/Orders.json"+ queryParams)
      .then(res => {
        for (let keyOrd in res.data) {
          res.data[keyOrd].key = keyOrd;
          orders.push(res.data[keyOrd])
        }
        dispatch(fetchOrederSuccessfull(orders))
      })
      .catch (err => {
        dispatch(fetchOrderFaild(err))
      })
    }
}