import axios from "axios";
import * as actionTypes from "./actionTypes";

export const authentication_start = () => {
    return {
        type: actionTypes.AUTHENTICATION_START
    };
};

export const authentication_successfull = (responseData) => {
    return {
        type: actionTypes.AUTHENTICATION_SUCCESSFUL,
        idToken: responseData.idToken,
        refreshToken: responseData.refreshToken,
        expiresIn: responseData.expiresIn,
        localId: responseData.localId
    };
};

export const logoutExpireTimer = (expiresIn)=>{
    return dispatch =>{
        setTimeout(()=>{
            dispatch(authertication_logout())
        },expiresIn * 1000)
    }

};

export const authentication_faild = (errMess) => {
    return {
        type: actionTypes.AUTHENTICATION_FAILD,
        errMess: errMess
    };
};

export const authertication_logout = ()=>{
    localStorage.removeItem('idToken')
    localStorage.removeItem('localId')
    localStorage.removeItem('expireDate')
    return {
        type:actionTypes.AUTHENTICATION_LOGOUT,
    }
};

export const authentication_started = (requestData, signInURL) => {
    const postUrl = signInURL ? 
    'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyAzCbcB4WGDdv4b-iTfd3HNe4hu0eDFrnw'
    :
    'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyAzCbcB4WGDdv4b-iTfd3HNe4hu0eDFrnw' 
    ;
    return dispatch => {
        dispatch(authentication_start())
        axios.post(postUrl,
            requestData)
            .then(response => {
                dispatch(authentication_successfull(response.data))
                dispatch(logoutExpireTimer(response.data.expiresIn))
                localStorage.setItem('idToken', response.data.idToken)
                localStorage.setItem('localId', response.data.localId)
                const expireDate = new Date(new Date().getTime() + response.data.expiresIn *1000) 
                localStorage.setItem('expireDate', expireDate)
            })
            .catch(error => {
                dispatch(authentication_faild(error.response.data.error.message))
            })

    }
}; 

export const setAuthentucationRedirectPath = (path)=>{
    return {
        type:actionTypes.SET_AUTHENTICATION_REDIRECT_PATH,
        path:path
    }
};

export const checkLocalStorgeAuthState = ()=>{
    return dispatch =>{
        const token = localStorage.getItem('idToken');
        if (!token){
            return dispatch(authertication_logout());
        }else{
            const expireDate = new Date(localStorage.getItem('expireDate'));
            if (expireDate > new Date()){
                const data = {
                    idToken:token,
                    localId:localStorage.getItem('localId'),
                }
                dispatch(authentication_successfull(data))
                dispatch(logoutExpireTimer((expireDate.getTime()-new Date().getTime())/1000))
            }else{
                dispatch(authertication_logout())
            }
        }
    }
}